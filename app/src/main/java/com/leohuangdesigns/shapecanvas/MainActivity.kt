package com.leohuangdesigns.shapecanvas

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {


    /*buttons on the toolbar
    0 - select
    1 - erase
    2 - line
    3 - circle
    4 - square
    */
    var toolButtons: Array<ImageButton> = emptyArray()
    //imageButtons do not toggle, so we have to implement a toggle state ourselves
    var toolPressedState = arrayOf(false, false, true, false, false)
    var toolEnabledState = arrayOf(true, false, true, true, true)

    /*colors in the palette
    0 - red
    1 - yellow
    2 - blue
    3 - green
    */
    var colorButtons: Array<ImageButton> = emptyArray()

    var appCanvas: CanvasView? = null


    override fun onSaveInstanceState(outState: Bundle) {
        for (i in 0..4) {
            outState.putBoolean("Pressed " + i, toolPressedState[i])
            outState.putBoolean("Enabled " + i, toolEnabledState[i])
        }
        outState.putInt("Current Color", appCanvas?.currentColor!!)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //set up arrays of buttons
        colorButtons = arrayOf(findViewById(R.id.btnRed), findViewById(R.id.btnYellow), findViewById(R.id.btnBlue),
            findViewById(R.id.btnGreen))
        toolButtons = arrayOf(findViewById(R.id.btnSelect), findViewById(R.id.btnErase), findViewById(R.id.btnLine),
            findViewById(R.id.btnCircle), findViewById(R.id.btnSquare))
        //setup canvas
        appCanvas = findViewById(R.id.mainCanvas)
        if (savedInstanceState != null) {
            for (i in 0..4) {
                toolPressedState[i] = savedInstanceState.getBoolean("Pressed " + i)
                toolEnabledState[i] = savedInstanceState.getBoolean("Enabled " + i)
            }
            restoreButtons(savedInstanceState.getInt("Current Color"))
        }
    }

    //change buttons to reflect state
    fun restoreButtons(color: Int) {
        for (i in 0..4) {
            if (toolPressedState[i]) {
                toolButtons[i].foreground = getDrawable(R.drawable.pressed)
            } else if (!toolEnabledState[i]) {
                toolButtons[i].foreground = getDrawable(R.drawable.disabled)
            } else {
                toolButtons[i].foreground = getDrawable(android.R.color.transparent)
            }
        }
        if (color == Color.RED) {
            showActiveColor(0)
        } else if (color == Color.YELLOW) {
            showActiveColor(1)
        } else if (color == Color.BLUE) {
            showActiveColor(2)
        } else if (color == Color.GREEN) {
            showActiveColor(3)
        }
    }

    //change the images on tool button corresponding to index target to reflect it's pressed status
    //also switch the other buttons off, meaning their images are changed and their toolPressedState set to false
    fun showActiveTool(target: Int) {
        if (toolEnabledState[target]) {
            toolPressedState[target] = !toolPressedState[target]
            if (toolPressedState[target]) {
                for (i in 0..4) {
                    if (i == target) {
                        toolButtons[i].foreground = getDrawable(R.drawable.pressed)
                    } else {
                        toolButtons[i].foreground = getDrawable(android.R.color.transparent)
                        toolPressedState[i] = false
                        toolEnabledState[i] = true
                    }
                }
                //pressing any button deselects the selected shape, so reset enabled/disabled buttons
                toolButtons[1].foreground = getDrawable(R.drawable.disabled)
                toolEnabledState[1] = false
            } else {
                toolButtons[target].foreground = getDrawable(android.R.color.transparent)
            }
        }
        appCanvas?.switchTool(target)
    }

    //change the images on color button corresponding to index target to reflect it's pressed status
    //also switch the other buttons off, meaning their images are changed and their colorPressedState set to false
    fun showActiveColor(target: Int) {
        for (i in 0..3) {
            if (i == target) {
                colorButtons[i].foreground = getDrawable(R.drawable.pressed)
            } else {
                colorButtons[i].foreground = getDrawable(android.R.color.transparent)
            }
        }
        appCanvas?.switchColor(target)
    }

    //called when the user has selected a shape, update toolbar accordingly
    //param is the color of the selection
    fun registerSelection(newColor: Int) {
        toolEnabledState[1] = true
        toolEnabledState[2] = false
        toolEnabledState[3] = false
        toolEnabledState[4] = false
        toolButtons[1].foreground = getDrawable(android.R.color.transparent)
        toolButtons[2].foreground = getDrawable(R.drawable.disabled)
        toolButtons[3].foreground = getDrawable(R.drawable.disabled)
        toolButtons[4].foreground = getDrawable(R.drawable.disabled)
        if (newColor == Color.RED) {
            showActiveColor(0)
        } else if (newColor == Color.YELLOW) {
            showActiveColor(1)
        } else if (newColor == Color.BLUE) {
            showActiveColor(2)
        } else if (newColor == Color.GREEN) {
            showActiveColor(3)
        }
    }

    //called when the user has deselected a shape, update toolbar accordingly
    fun registerDeselection() {
        toolEnabledState[1] = false
        toolEnabledState[2] = true
        toolEnabledState[3] = true
        toolEnabledState[4] = true
        toolButtons[1].foreground = getDrawable(R.drawable.disabled)
        toolButtons[2].foreground = getDrawable(android.R.color.transparent)
        toolButtons[3].foreground = getDrawable(android.R.color.transparent)
        toolButtons[4].foreground = getDrawable(android.R.color.transparent)
    }

    //when the select button is pressed
    fun pressSelect(view: View) {
        showActiveTool(0)
    }

    //when the erase button is pressed
    fun pressErase(view: View) {
        appCanvas?.deleteSelection()
    }

    //when the line button is pressed
    fun pressLine(view: View) {
        showActiveTool(2)
    }

    //when the circle button is pressed
    fun pressCircle(view: View) {
        showActiveTool(3)
    }

    //when the square button is pressed
    fun pressSquare(view: View) {
        showActiveTool(4)
    }

    //when the color red is picked
    fun pressRed(view: View) {
        showActiveColor(0)
    }

    //when the color yellow is picked
    fun pressYellow(view: View) {
        showActiveColor(1)
    }

    //when the color blue is picked
    fun pressBlue(view: View) {
        showActiveColor(2)
    }

    //when the color green is picked
    fun pressGreen(view: View) {
        showActiveColor(3)
    }

}
