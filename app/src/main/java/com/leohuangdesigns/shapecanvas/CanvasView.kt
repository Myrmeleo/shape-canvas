package com.leohuangdesigns.shapecanvas
import android.graphics.Canvas
import android.view.MotionEvent
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.graphics.Color
import android.graphics.Paint
import kotlin.math.*


class CanvasView (context: Context, attributes: AttributeSet? = null) :
    View(context, attributes) {

    abstract class CShape {
        abstract var color: Int
        //whether the touch coords are in the shape
        abstract fun contains(touchX: Float, touchY: Float): Boolean
        //move the shape according to the touch coords
        abstract fun translate(touchX: Float, touchY: Float)
        //stretch the shape as the user is drawing it
        abstract fun stretch(touchX: Float, touchY: Float)
        //put the shape on the canvas
        abstract fun draw(canvas: Canvas, paint: Paint)
        //highlight it if it's selected
        abstract fun drawHighlight(canvas: Canvas, paint: Paint)
    }

    //line drawing
    //fields are the coords of the two endings
    class Line (override var color: Int, var startX: Float, var startY: Float, var endX: Float, var endY: Float): CShape() {

        //how far the touch can go from the line while still being detected
        val tolerance = 20

        //line is determined to be clicked if touch coordinate is within tolerance of nearest point
        override fun contains(touchX: Float, touchY: Float): Boolean {
            val lengthSqr = ((startX - endX).pow(2) + (startY - endY).pow(2))
            var distance: Float
            val distTouchToStartPoint = sqrt((touchX - startX).pow(2) + (touchY - startY).pow(2))
            if (lengthSqr == 0f) {
                distance = distTouchToStartPoint
            } else {
                var t = ((touchX - startX) * (endX - startX) + (touchY - startY) * (endY - startY)) / lengthSqr
                t = max(0f, min(1f, t))
                val closestX = startX + t * (endX - startX)
                val closestY = startY + t * (endY - startY)
                distance = sqrt((closestX - touchX).pow(2) + (closestY - touchY).pow(2))
            }
            return distance <= tolerance
        }

        //move line
        override fun translate (touchX: Float, touchY: Float) {
            //set finger to be the midpoint of the line
            val halfDeltaX = (endX - startX) / 2
            val halfDeltaY = (endY - startY) / 2
            startX = touchX - halfDeltaX
            endX = touchX + halfDeltaX
            startY = touchY - halfDeltaY
            endY = touchY + halfDeltaY
        }

        override fun stretch (touchX: Float, touchY: Float) {
            endX = touchX
            endY = touchY
        }

        override fun draw(canvas: Canvas, paint: Paint) {
            paint.color = color
            canvas.drawLine(startX, startY, endX, endY, paint)
        }

        override fun drawHighlight(canvas: Canvas, paint: Paint) {
            paint.color = Color.CYAN
            paint.style = Paint.Style.STROKE
            canvas.drawLine(startX, startY, endX, endY, paint)
        }
    }


    //circle drawing
    class Circle (override var color: Int, var topLeftX: Float, var topLeftY: Float, var centerX: Float, var centerY: Float, var radius: Float): CShape() {
        //circle is determined to be clicked if touch coordinate is within radius of center
        override fun contains(touchX: Float, touchY: Float): Boolean {
            val deltaX: Double = (touchX - centerX).toDouble()
            val deltaY: Double = (touchY - centerY).toDouble()
            val deltaR = sqrt(deltaX.pow(2) + deltaY.pow(2))
            return deltaR <= radius
        }

        //move circle
        override fun translate (touchX: Float, touchY: Float) {
            topLeftX += touchX - centerX
            topLeftY += touchY - centerY
            centerX = touchX
            centerY = touchY
        }

        override fun stretch (touchX: Float, touchY: Float) {
            radius = max(abs(touchX - topLeftX), abs(touchY - topLeftY)) / 2
            centerX = (topLeftX + touchX) / 2
            centerY = (topLeftY + touchY) / 2
        }

        override fun draw(canvas: Canvas, paint: Paint) {
            paint.color = color
            canvas.drawCircle(centerX, centerY, radius, paint)
        }

        override fun drawHighlight(canvas: Canvas, paint: Paint) {
            paint.color = Color.CYAN
            paint.style = Paint.Style.STROKE
            canvas.drawCircle(centerX, centerY, radius, paint)
        }
    }

    //square drawing
    //fields are the coords of the top left corner and the width and height
    class Square (override var color: Int, var startX: Float, var startY: Float, var width: Float, var height: Float): CShape() {
        //square is determined to be clicked if touch coordinate is within box
        override fun contains(touchX: Float, touchY: Float): Boolean {
            var inXBounds: Boolean
            var inYBounds: Boolean
            if (width > 0) {
                inXBounds = touchX >= startX && touchX <= startX + width
            } else {
                inXBounds = touchX <= startX && touchX >= startX + width
            }
            if (height > 0) {
                inYBounds = touchY >= startY && touchY <= startY + height
            } else {
                inYBounds = touchY <= startY && touchY >= startY + height
            }
            return inXBounds && inYBounds
        }

        //move square
        override fun translate (touchX: Float, touchY: Float) {
            val halfWidth = width / 2
            val halfHeight = height / 2
            startX = touchX - halfWidth
            startY = touchY - halfHeight
        }

        override fun stretch (touchX: Float, touchY: Float) {
            width = touchX - startX
            height = touchY - startY
        }

        override fun draw(canvas: Canvas, paint: Paint) {
            paint.color = color
            canvas.drawRect(startX, startY, startX + width, startY + height, paint)
        }

        override fun drawHighlight(canvas: Canvas, paint: Paint) {
            paint.color = Color.CYAN
            paint.style = Paint.Style.STROKE
            canvas.drawRect(startX, startY, startX + width, startY + height, paint)
        }
    }

    //don't put erase in this enum since it's not a tool that stays active after being pressed
    enum class Tool {SELECT, LINE, CIRCLE, SQUARE}
    var currentTool: Tool = Tool.LINE
    //one of two things happen when the user touches the canvas
    enum class Action {DRAW, SELECT}
    var currentAction = Action.DRAW

    //shape in the middle of being drawn
    var shapeInProgress: CShape? = null
    //currently selected shape
    var currentSelect: CShape? = null

    //shapes on the canvas right now
    var shapesOnCanvas: ArrayList<CShape> = arrayListOf()
    //paint for drawing the shapes
    var paint = Paint()

    var colors = arrayOf(Color.RED, Color.YELLOW, Color.BLUE, Color.GREEN)
    var currentColor = colors[0]

    init {
        paint.strokeWidth = 10f
    }

    fun switchTool(target: Int) {
        if (target == 0) {
            currentTool = Tool.SELECT
            currentAction = Action.SELECT
        } else if (target == 2) {
            currentTool = Tool.LINE
            currentAction = Action.DRAW
        } else if (target == 3) {
            currentTool = Tool.CIRCLE
            currentAction = Action.DRAW
        } else if (target == 4) {
            currentTool = Tool.SQUARE
            currentAction = Action.DRAW
        }
    }

    fun switchColor(target: Int) {
        currentColor = colors[target]
        if (currentSelect != null) {
            currentSelect?.color = colors[target]
            invalidate()
        }
    }

    //create the shape to be drawn
    fun beginDrawing(touchX: Float, touchY: Float) {
        if (currentTool == Tool.LINE) {
            shapeInProgress = Line(currentColor, touchX, touchY, touchX, touchY)
        } else if (currentTool == Tool.CIRCLE) {
            shapeInProgress = Circle(currentColor, touchX, touchY, touchX, touchY, 0f)
        } else if (currentTool == Tool.SQUARE) {
            shapeInProgress = Square(currentColor, touchX, touchY,0f, 0f)
        }
    }

    //delete the selected shape
    fun deleteSelection() {
        if (currentSelect != null) {
            shapesOnCanvas.remove(currentSelect!!)
            currentSelect = null
            (context as MainActivity).registerDeselection()
        }
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        for (i in shapesOnCanvas) {
            i.draw(canvas, paint)
        }
        if (shapeInProgress != null) {
            shapeInProgress?.draw(canvas, paint)
        }
        if (currentSelect != null) {
            currentSelect?.drawHighlight(canvas, paint)
            paint.style = Paint.Style.FILL
        }
    }

    //detect touch and handle depending on how the touch event
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        //get coords
        val x = ev.x
        val y = ev.y
        when (ev.action) {
            MotionEvent.ACTION_DOWN -> {
                /*when user begins touching the screen, 1 of 2 things can happen
                1: draw a shape
                2: select a drawn shape
                */
                if (currentAction == Action.DRAW) {
                    beginDrawing(x, y)
                } else {
                    for (i in shapesOnCanvas) {
                        if (i.contains(x, y)) {
                            currentSelect = i
                            currentColor = i.color
                            (context as MainActivity).registerSelection(i.color)
                            break
                        } else {
                            //if user presses the canvas, deselect
                            currentSelect = null
                            (context as MainActivity).registerDeselection()
                        }
                    }
                }
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                /*when user slides a finger across the screen, 1 of 2 things is happening
                1: A shape in the middle of being drawn is having its dimensions determined
                2: A selected shape is being moved
                */
                if (currentAction == Action.DRAW) {
                    shapeInProgress?.stretch(x, y)
                } else {
                    if (currentSelect != null) {
                        currentSelect?.translate(x, y)
                    }
                }
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                /*when user lifts their finger, 1 of 2 things can happen
                1: A shape in the middle of being drawn is completed
                2: A selected shape is put into its destination
                */
                if (currentAction == Action.DRAW) {
                    if (shapeInProgress != null) {
                        shapesOnCanvas.add(shapesOnCanvas.size, shapeInProgress!!)
                        shapeInProgress = null
                    }
                } else {
                }
                invalidate()
            }
        }
        return true
    }
}