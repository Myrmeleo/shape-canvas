Leo Huang 2019  
Made with Android Studio  

Implemented a drawing canvas with tools for lines, rectangles, and circles, as well as selection and deletion.  

To run, manually install the file app-debug.apk onto your Android device.  